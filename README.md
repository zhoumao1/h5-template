宣传网址: https://h5.shensx.com/insurances/?insur_code=ZHBWYY
客服网址: https://h5.shensx.com/insurances/dialog_chat?insur_code=ZHBWYY&channel_code=insurance=&source_code=h5


兼容低版本配置
npm i babel-polyfill es6-promise -S
npm i @babel/plugin-transform-runtime @babel/preset-env -D
.babelrc 文件
	```JSON
	{
   	"presets": [
   		"@babel/preset-env"
   	],
   	"plugins": [
   		"@babel/plugin-transform-runtime"
   	]
   }

	```

.babel.config.js 文件
	```JavaScript
	presets: [
		//"@vue/cli-plugin-babel/preset",
		/*"es2015"*/
		// 兼容低版本
		[
			'@vue/app',
			{
				'useBuiltIns': 'entry',
				polyfills: [
					'es6.promise',
					'es6.symbol'
				]
			}
		]
	],

	```
