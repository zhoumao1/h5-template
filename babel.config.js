module.exports = {
	presets: [
		//"@vue/cli-plugin-babel/preset",
		/*"es2015"*/
		// 兼容低版本
		[
			'@vue/app',
			{
				'useBuiltIns': 'entry',
				polyfills: [
					'es6.promise',
					'es6.symbol'
				]
			}
		]
	],
	// plugins: ["transform-vue-jsx"]
};
