import ParentView from '_c/main/parent-view'

export default {
	path: "/",
	component: ParentView,
	children: [
		{
			path: "/",
			name: 'insurance',
			component: () => import("@/views/insurance/insurance")
		}
	]
}
