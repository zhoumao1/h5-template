/**
 * @type {Array<InsuranceListProps>}
 */
export const insuranceList = [
	{
		insur_code: 'ZHBWYY',
		title: '肾爱保-日常住院险',
		buring_info: {
			click_entry_code: 'ClickZHBWYYCEntry',
			open_servece_code: 'OpenZHBWYYCustomerService'
		},
		insur_web_url: 'https://channelh5.pubmi.cn/kidney/inhospitalDetail?channel=ZT19092901&branchNo=shensx10000&branchName=%E8%82%BE%E4%B8%8A%E7%BA%BF&flag=Y&source=shensx10000',
		share_info: {
			title: '肾上线推荐！报销平时住院费用的肾友保险',
			desc: '100种疾病！门诊手术，住院，特殊门诊治疗均可报销！',
			thumb: 'http://static.ishenzang.com/static/ko/h5/insurance/zhbwyy_logo.jpg'
		}
	},
	{
		insur_code: 'axskb',
		title: '爱心-肾康保',
		buring_info: {
			click_entry_code: 'ClickAXSKBEntry',
			open_servece_code: 'OpenAXSKBCustomerService'
		},
		insur_web_url: 'https://ow.aixinwechat.com/brokerage-product/F1223537F5B70BAD?otherCode=AKD01001',
		share_info: {
			title: '首款给30万现金的长期肾病险，一步保到70岁！',
			desc: '买入就0风险',
			thumb: 'http://static.ishenzang.com/static/ko/h5/insurance/axskb_logo.jpg'
		}
	},
]

/**
 * @typedef InsuranceListProps
 * @property   {string}   insur_code   保险区分code
 * @property   {string}   title        网页标题
 * @property   {string}   insur_web_url        保险网站地址
 * @property   {InsuranceBuringInfoProps}   buring_info        埋点信息
 * @property   {InsuranceShareProps}   share_info        分享信息
 */
/**
 * @typedef InsuranceBuringInfoProps
 * @property   {string} click_entry_code 进入网页 code
 * @property   {string} open_servece_code 进入对话 code
 */
/**
 * @typedef InsuranceShareProps
 * @property   {string} title 分享的标题
 * @property   {string} desc 分享的描述
 * @property   {string} thumb 分享的缩略图
 */

