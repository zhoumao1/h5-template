import axios from '@/api';

/**@see http://doc.shensx.com/project/108/interface/api/4380  根据微信UnionID获取患者详情*/
export function GetPatientByUnionId(data) {
	return axios({
		method: 'get',
		url: '/micro/Insurance/microapi/InsuranceApi/GetPatientByUnionId',
		params: data
	})
}

/**@see http://doc.shensx.com/project/108/interface/api/4377  微信报名接口*/
export function SignUpForWx(data) {
	return axios({
		method: 'post',
		url: '/micro/Insurance/microapi/InsuranceApi/SignUpForWx',
		data: { data }
	})
}

/**@see http://doc.shensx.com/project/108/interface/api/4389  微信测评接口*/
export function EvaluationForWx(data) {
	return axios({
		method: 'post',
		url: '/micro/Insurance/microapi/InsuranceApi/EvaluationForWx',
		data: { data }
	})
}
