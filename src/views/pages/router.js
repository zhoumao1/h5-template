import ParentView from '@/components/main/parent-view'

export default {
	path: '/home',
	component: ParentView,
	children: [
		{
			path: '',
			name: 'home',
			component: () => import('./home')
		},
		{
			path: 'page_1',
			name: 'page-1',
			meta: {
				is_login: false
			},
			component: () => import('./page-1')
		},
		{
			path: 'page_2',
			name: 'page-2',
			meta: {
				is_login: false
			},
			component: () => import('./page-2')
		},
		{
			path: 'page_3',
			name: 'page-3',
			meta: {
				is_login: false
			},
			component: () => import('./page-3')
		},
	]
}
