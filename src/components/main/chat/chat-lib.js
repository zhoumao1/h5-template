import { GeneratePresignedUri } from '@/api/public';

import {getTime} from '@/libs/dateFormater';
import { ImagePreview } from 'vant';

export default {
	created(){
	},
	filters: {
		f_GetTime(tdate, format) {
			return (typeof tdate !== 'number' && !tdate) ? '' : getTime(tdate, format)
		},
		f_Age(birthday) {
			let birthDayTime = new Date(birthday * 1000).getTime();
			let nowTime = new Date().getTime();
			return Math.ceil((nowTime-birthDayTime)/31536000000);
		},
		// 性别
		f_Gender: function (value) {
			if (value === 'f') {
				return '女';
			} else if (value === 'm') {
				return '男';
			} else {
				return 'NA';
			}
		},
	},
	methods: {
		delayFn(delay_time) {
			let delayFlagIndex = 23
			return this.hasMsgAnimate && this.hasMsgDelayAnimate ? `${ (delay_time >delayFlagIndex ? (delay_time % delayFlagIndex):0) * 0 }s` : 0
		},
		/**
		 * 处理消息 加载动画
		 * @param msgList {Array} 消息列表
		 * @param has_animate {boolean} 是否需要动画(defalut: true)
		 * @return msgList {Array} 处理后的消息
		 */
		handleAnimateFn(msgList, has_animate = this.hasMsgAnimate, flag) {
			return msgList.map(msg => ({ ...msg, load_msg_animate: has_animate }))
		},
		/**
		 * 获取图片
		 * @param  d        { [Object] } 传入带有图片的数组对象 [{}]
		 * @param  img_key  { string }  图片字段, 如没有则不传
		 * @return d        { [Object] } 处理后的图片[图片key: img_url]
		 */
		async getImageUrl(d, img_key) {
			let arr = [];
			// Undefined 或者 空数组
			if (!d || d.length <= 0) return d;

			try {
				if(d.some(item => typeof item === 'object') && !img_key){
					console.error('缺少 image 字段')
					return
				}
				d.forEach(item => {
					arr.push({
						osskey: typeof item === 'object' ? (item[img_key] || '') : item,
						level: 1
					});
				});
				let { data: imgUrl } = await GeneratePresignedUri(arr);

				if(arr.length > 0){
					if (img_key) {
						imgUrl.forEach((item, index) => {
							if (item.osskey) {
								// d[index][img_key+'_url'] = item.url
								this.$set(d[index], [img_key+'_url'], item.url)
							}
						})
					}else {
						for (let i = 0; i < d.length; i ++) {
							// d[i] = imgUrl[i]['url'];
							this.$set(d, i, imgUrl[i]['url'])
						}
					}
				}

				return d
			} catch (e) {
				console.error('e', e);
				return d
			}
		},
		/// H5
		viewImage(img_url){
			ImagePreview({
				images: [img_url],
				closeOnPopstate: true
			});
		}

	}
}
