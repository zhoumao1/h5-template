import { SubmitDataBuringInfo } from '@/api/public';

export const MIX_INSURANCE = {
	// props: {
	// 	insurCode: String,
	// 	/**@property {InsuranceListProps}   insuranceInfo*/
	// 	insuranceInfo: Object
	// },
	// created(){
	// 	if(this.$store.getters.apiToken && this.insurCode) {
	// 		this.handleSubmitDataBuringInfo(this.insuranceInfo.buring_info.click_entry_code)
	// 	}
	// },
	methods: {
		handleSubmitDataBuringInfo(event_code, extra_info){
			SubmitDataBuringInfo({
				event_code,
				extra_info: Object.assign({
					channel_code: this.$store.getters.isAppEntry ? 'app' : 'h5'
				}, extra_info)
			})
		}
	}
}
