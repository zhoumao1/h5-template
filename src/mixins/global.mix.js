import { GeneratePresignedUri } from '@/api/public';
import { getTime } from '@/libs/dateFormater'
import Vue from 'vue'
import { getObjectValue, isEmpty, setObjectValue } from 'vue-blur-load/src/tools';

export const MIX_GLOBAL = {
	filters: {
		f_GetTime(time, format) {
			return getTime(time * 1000, format || 'YYYY-MM-DD HH:mm:ss')
		}
	},
	computed: {
		shareURL(){
			return process.env.VUE_APP_SHARE
		},
		toolURL(){
			return process.env.VUE_APP_TOOL
		}
	},
	methods: {
		/**
		 * 获取图片
		 * @param  d        { [Object] } 传入带有图片的数组对象 [{}]
		 * @param  img_key  { string }  图片字段, 如没有则不传, 同时存在多张照片用逗号分割
		 * @example img_key
		 * 支持类型: key, path_key <br/>
		 * var obj = { name: 'xxx', address: { cities: [{ city: '山西' }], test: '111' } } <br/>
		 * key: 'name' === obj.name <br/>
		 * path_key: address.cities[0].city === obj.address.cities[0].city
		 * @param  def_pic  { boolean|string }  是否需要默认的图片
		 * @param level     {number<1|2|>}              图片清晰度
		 * @return d        { [Object] } 处理后的图片[图片key: img_url]
		 */
		async GetImgUrlFn(d, img_key = '', def_pic = '', level = 1) {
			let arr = [];
			let $set = Vue.set
			if(isEmpty(d))return d

			try {
				if(d.some(item => typeof item === 'object') && !img_key) {
					console.error('缺少 image 字段')
					return
				}
				d.forEach(item => {
					img_key.split(',')
						.map(item => item.trim())
						.forEach(img_key_item => {
							arr.push({
								osskey: typeof item === 'object' ? (getObjectValue(item, img_key_item, '')) : item,
								level
							});
						})
				});
				let { data: imgUrl } = await GeneratePresignedUri(arr);
				// console.log(imgUrl, 'imgUrl')

				if(!arr.length) return d
				if(img_key) {
					d.forEach(item => {
						img_key.split(',')
							.map(item => item.trim())
							.forEach(img_key_item => {
								let tmpFindObj = imgUrl.find(img_url_item => img_url_item.osskey === getObjectValue(item, img_key_item, null))
								// $set(item, img_key_item + '_url', tmpFindObj && tmpFindObj.url)
								setObjectValue(item, img_key_item + '_url', tmpFindObj && tmpFindObj.url, $set)
								if(def_pic) {
									let emtryImg = imgUrl.find(img_url_item => !img_url_item.osskey)
									setObjectValue(item, img_key_item + '_url', typeof def_pic ==='boolean' ? def_pic:emtryImg, $set)
									// $set(item, img_key_item + '_url', typeof def_pic ==='boolean' ? def_pic:emtryImg)
								}
								// if(!def_pic && tmpFindObj) {
								// 	$set(item, img_key_item + '_url', tmpFindObj.url)
								// } else if(def_pic && !tmpFindObj) {
								// 	$set(item, img_key_item + '_url', def_pic)
								// }
							})
					})
				} else {
					for (let i = 0; i < d.length; i ++) {
						$set(d, i, imgUrl[i]['url'])
					}
				}
				return d
			} catch (e) {
				console.error('e', e);
				return d;
			}
		},
		/**
		 * 获取更深一级的 img 地址
		 * @param data       {[]}     大数组
		 * @param list_name  {string} 大数组中包含图片的 父节点key
		 * @param img_key    {string} 图片key
		 * @param id         {string} 大数组中每个item 的标识符, 用于赋值
		 * @param def_pic { boolean|string }  是否需要默认的图片
		 * @return data {Promise}          返回带有图片的 大数组, 结构不变
		 */
		async GetDeepImgUrl(data, list_name, img_key, id, def_pic = '') {
			if (data.length <= 0) return []
			if (!list_name) throw 'list_name: 包含图片的父节点 key 是必填的'
			if (!id) throw 'id: 用于最后赋值, 必传'

			let arr = []
			arr = data.map(data_item => {
				let isArray = Object.prototype.toString.call(data_item[list_name])
				.includes('Array')
				if (isArray) {
					return data_item[list_name].map(item => {
						return {
							osskey: typeof item === 'object' ? (item[img_key] || '') : item,
							level: 1
						}
					})
				} else {
				}
			})
			.flat(Infinity)
			.filter(item => item)
			let { data: imgUrl } = await GeneratePresignedUri(arr);

			data.forEach(data_item => {
				let isArray = Object.prototype.toString.call(data_item[list_name])
				.includes('Array')
				if (isArray) {
					if (img_key) {
						data_item[list_name].forEach(item => {
							let tmpFindObj = imgUrl.find(img_url_item => img_url_item.osskey === item[img_key])
							this.$set(item, img_key + '_url', tmpFindObj && tmpFindObj.url)
							if (!def_pic && tmpFindObj) {
								this.$set(item, img_key + '_url', tmpFindObj.url)
							}else if (def_pic && !tmpFindObj){
								this.$set(item, img_key + '_url', def_pic)
							}
						})
					} else {
						for (let i = 0; i < data_item.length; i++) {
							this.$set(data_item, i, imgUrl[i]['url'])
						}
					}
				}
			})

			return data

		},
	}
}
Vue.mixin(MIX_GLOBAL)
