import ParentView from '@/components/main/parent-view'

export const routes = [
	//微信回调页面
	{
		path: "/wxCallback",
		name: "wxCallback",
		component: () => import("@/views/wx/wxCallback.vue"),
		meta: {
			index: 1,
			requireAuth: 0
		}
	},
	//绑定手机号
	{
		path: "/bindTel",
		name: "bindTel",
		component: () => import("@/views/wx/bindTel.vue"),
		meta: {
			index: 1,
			requireAuth: 0
		}
	},
	//医生勿登录
	{
		path: "/doctorlogin",
		name: "doctorlogin",
		component: () => import("@/views/wx/doctorlogin.vue"),
		meta: {
			index: 1,
			requireAuth: 0
		}
	},

];
let contextList = require.context('../views', true, /router\.js$/)
export const asyncRoutes = contextList.keys().map(item => contextList(item).default || contextList(item))

