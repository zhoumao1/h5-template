import Vue from "vue";
import Router from "vue-router";
import { AndroidBack, BackHandle, wechatFlag } from '@/libs/browser';
import store from '@/store';
import { asyncRoutes, routes } from '@/router/router-list';
import { baseRoute } from '@/settings';
import { handleUserAuth } from '@/router/auth-tool';
import { insuranceList } from '_v/insurance/insurance-list';
import { param2Obj } from '@/libs/tools';

Vue.use(Router);
const BASE_URL = baseRoute
// 是否为app登录 有 code 并且 不是微信浏览器
const HAS_LOGIN_PARAMS = !!(param2Obj().code && param2Obj().code_type)
const APPLOGIN = (HAS_LOGIN_PARAMS || (store.getters.isAppEntry)) && !(wechatFlag().is_wechat)
// alert(APPLOGIN)
const createRouter = () => new Router({
	base: BASE_URL,
	mode: 'history',
	// 微信登录的话 使用 routes.concat(asyncRoutes)
	// APP登录 使用 asyncRoutes,  需动态添加 路由
	routes: APPLOGIN ? asyncRoutes : routes.concat(asyncRoutes)
})
export const router = createRouter()

export function resetRouter() {
	const newRouter = createRouter()
	router.matcher = newRouter.matcher // reset router
}

// 自定义 路由回退事件
const routerBack = router.back.bind(router);
router.back = () => {
	let homePath = store.getters.homeRoute.path
	// 路由判断 在有 children 的情况下不严谨, 可能会出现意外退出的问题
	// 尽量不用使用 children
	/*console.log(location.pathname.includes(homePath), '是否可以退出')
	console.log(location.pathname, '----', homePath, '路径查看')*/
	if (location.pathname.includes(homePath) && homePath) {
		if (APPLOGIN) {
			new BackHandle().closeWebView()
		} else {
			// console.error('关闭h5', history.state)
			// new BackHandle().closeH5Page(true)
		}

	}

	routerBack();
};
// 全局路由拦截器
router.beforeEach(async (to, from, next) => {
	// console.warn(to, from, router, APPLOGIN, 'beforeEach')
	if (to.meta.title) {
		//判断是否有标题
		document.title = to.meta.title
	}
	if (to.query.insur_code) {
		document.title = insuranceList.find(item => item.insur_code.toLowerCase() === to.query.insur_code.toLowerCase()).title
	}
	// app登录 || 有token

	if (APPLOGIN) {
		if (to.path === from.path) {
			new BackHandle().closeWebView()
		}
		// debugger
		// 安卓
		if (store.getters.hasAuth) {
			next()
		} else {
			if (param2Obj().code && param2Obj().code_type) {
				store.commit('M_CODE_OR_CODE_TYPE', {
					code: param2Obj().code,
					code_type: param2Obj().code_type
				})
			}
			// console.error('首次进入')
			// debugger
			// console.log(to.path, '初次进入的地址')
			store.commit('M_HOME_ROUTE', to)
			await store.dispatch('appLogin')

			next()
			// next({ ...to, replace: true })
		}
		return
	}

	if (!wechatFlag().is_wechat) {
		alert("请使用微信打开");
		return;
	}

	if (location.href.includes('first_enter') && to.query.first_enter) {
		// 初次进入 进行压栈处理(以便监控到返回操作)
		if (!history.state) {
			console.log(history.state, 'first_enter')
			const Back = new BackHandle()
			Back.push({ flag: 'first-enter-flag' })
		}
	}

	// 保险介绍页 不需要授权   跳转时候 再授权
	if (to.path.includes('insurances') || to.path === '/') {
		if (!to.query.first_enter) {
			store.commit('M_HOME_ROUTE', to)
			location.replace(window.location.protocol +
				"//" +
				window.location.host +
				"/" + BASE_URL + to.fullPath + '&first_enter=true');
		} else {
			next()
		}
		return
	}

	// console.log(location.href.includes('first_enter'), history.length, history.state, '历史长度')
	// token 校验
	// 一般to.meta.requireAuth为true时才进行校验
	// 这里只有wxCallback路由不需要认证，其requireAuth===0
	if (to.meta.requireAuth !== 0) {
		// 如果状态中没有token，进入微信登录页面
		let hasToken = await handleUserAuth({
			full_path: to.fullPath,
			path: to.path
		})
		if (hasToken) {
			next();
		} else {
			store.commit('M_HOME_ROUTE', to)
		}
	} else {
		next();
	}
});

router.afterEach((to, from) => {
	//统一设置分享
	let url =
		window.location.protocol +
		"//" +
		window.location.host +
		"/" + BASE_URL + "" +
		to.fullPath;

	// commonShareWx({
	// 	title: 'title',
	// 	link: url.split("#")[0],
	// 	desc: '描述',
	// 	img: 'tmpinsurance.share_info.thumb'
	// });
});

window.onbeforeunload = e => {      //刷新时弹出提示
	// console.warn(history.state, location.pathname, 'beforeunload')
	APPLOGIN && store.dispatch('resetToken')
};

if (!APPLOGIN) {
	// 处理正常返回退出
	window.addEventListener('popstate', () => {
		let homePath = store.getters.homeRoute.path
		// console.warn(history.state, location.pathname, homePath, 'popstate')
		if (!history.state) {
			let backHandle = new BackHandle()
			backHandle.closeH5Page(true, { flag: 'first-enter-flag' })
		}
	}, false)

	new AndroidBack()

	// window.addEventListener('pagehide', function () {
	// 	alert('pagehide')
	// })
}
export default router;
