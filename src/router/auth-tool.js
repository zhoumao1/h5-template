import store from '@/store'
import router from '@/router/router-index';
import { baseRoute } from '@/settings';

export function handleAuthRouter(name){
	if(store.getters.isAppEntry)return Promise.resolve(true)
	if(store.getters.loginData.wxloginstate){
		return Promise.resolve(true)
	}else {
		router.push({
			name: 'bindTel',
			query: {
				redirect_name: name
			}
		})
		return Promise.resolve(false)
	}
}

/**
 * 用户授权
 * @param {string}   full_path   类似于 $route.fullPath
 * @param {string}   path        类似于 $route.path
 */
export function handleUserAuth({full_path, path}){
	if(store.state.apiAuth.apiToken || store.getters.loginData.marketing){
		return Promise.resolve(true)
	}else {
		let appid = null;
		let urlDizi = null;
		if(process.env.VUE_APP_RUN_MODE === 'build_prod') {
			appid = "wx83105b4f6d8eaf56"; //打包线上
			urlDizi = "https://h5.shensx.com/" + baseRoute + "/wxCallback";
		} else if(process.env.VUE_APP_RUN_MODE === "build_test") {
			appid = "wx16f2fcc1304fa0f1"; //打包测试
			urlDizi = "http://testh5.shensx.com/" + baseRoute + "/wxCallback";
		} else if(process.env.VUE_APP_RUN_MODE === "build_dev") {
			urlDizi = "http://devh5.shensx.com/" + baseRoute + "/wxCallback";
		} else {
			urlDizi =
				window.location.protocol +
				"//" +
				window.location.host +
				"/" + baseRoute + "/wxCallback";
		}
		let redirect_uri = null;
		let state = null;
		let wxAuthUrl = "";
		// let queryStr = to.fullPath.split('?')[1]
		let queryStr = full_path.split('?')[1]
		queryStr && store.commit('M_REDIRECT_QUERY_STR', '?'+queryStr + '&first_enter=true')
		if (['build_test', 'build_prod'].find(item => item === process.env.VUE_APP_RUN_MODE)) {
			// if (process.env.VUE_APP_ALIKE === 'wxSignIn') {
			redirect_uri = escape(urlDizi);
			// state = escape(to.path);
			state = escape(path);
			wxAuthUrl =
				"https://open.weixin.qq.com/connect/oauth2/authorize" +
				"?appid=" +
				appid +
				"&redirect_uri=" +
				redirect_uri +
				"&response_type=code" +
				"&scope=snsapi_userinfo" +
				"&state=" +
				state +
				"" +
				"#wechat_redirect";
		} else {
			redirect_uri = encodeURI(urlDizi);
			// state = escape("path=" + to.path);
			state = escape("path=" + path);
			wxAuthUrl =
				"https://share.ishenzang.com/mp/BaseMP/RequestWechatOAuth?nextURLID=" +
				redirect_uri +
				"&routeParam=" +
				state;
		}
		// 直接跳转微信登录页面
		location.replace(wxAuthUrl);
		return Promise.resolve(false)
	}

}
