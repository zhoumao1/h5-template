import { LoginByWechatForH5 } from '@/api/wx';
import { BackHandle } from '@/libs/browser';
import Load from 'vue-blur-load/src/load';
import { Dialog, Toast } from 'vant';
import { baseRoute } from '@/settings';
import { cloneObj, deepMerge } from 'vue-blur-load/src/tools';
const TOKEN = baseRoute + '__token'
const state = {
	apiToken: window.sessionStorage.getItem(TOKEN),
	// 是否为app进入
	isAppEntry: false,
	/**@type {LoginDataProp}*/
	loginData: {},
	hasAuth: false,
	code: '',
	code_type: '',
	//用于判断是否到根路由(初始进入页面的路由)
	homeRoute: {
		meta: {},
		fullPath: '',
		name: '',
		path: '',
		query: {}
	},
	redirectQueryStr: ''
}
const mutations = {
	M_IsAppEntry: (state, flag) => {
		state.isAppEntry = flag
	},
	M_HAS_AUTH: (state, flag) => {
		state.hasAuth = flag
	},
	M_HOME_ROUTE: (state, route) => {
		if(!route){
			state.homeRoute = {}
		}else {
			cloneObj(state.homeRoute, route)
		}
	},
	setApiToken(state, data) {
		state.apiToken = data;
		window.sessionStorage.setItem(TOKEN, data)
	},
	M_LOGIN_DATA: (state, data) => {
		data = deepMerge(data)
		if(data.wxloginstate || data.wxloginstate < 1){
			data.wxloginstate = data.wxloginstate === 1
			if(data.wxloginstate){
				data.is_doctor = data.userrole === 1
			}
		}
		console.log(data, 'datadatadatadatadatadata')
		state.loginData = data
	},
	/**
	 * @param {state} state
	 * @param {{code: string, code_type: string}|undefined}   info
	 */
	M_CODE_OR_CODE_TYPE(state, info){
		if(!info){
			state.code = state.code_type = ''
			return
		}
		state.code = info.code
		state.code_type = info.code_type
	},
	M_REDIRECT_QUERY_STR(state, query_str){
		state.redirectQueryStr = query_str
	}
}
const actions = {
	setApiToken({ commit }, token) {
		commit('setApiToken', token)
	},
	// remove token
	resetToken({ commit }) {
		return new Promise(resolve => {
			commit('setApiToken', '')
			commit('M_HAS_AUTH', false)
			commit('M_CODE_OR_CODE_TYPE');
			resolve()
		})
	},
	// app登录 免授权
	async appLogin({ state, commit, dispatch }) {
		return new Promise((resolve, reject) => {
			if(!state.code) {
				Toast.fail('ERR: 登录参数')
				return
			}
			Load.show()
			LoginByWechatForH5({
				code: state.code,
				code_type: state.code_type
			})
				.then(async ({ data: loginData }) => {
					Load.close()
					commit('M_LOGIN_DATA', loginData)
					// console.log(loginData, 'loginData')
					dispatch('setApiToken', loginData.token);
					commit('M_CODE_OR_CODE_TYPE', {
						code: state.code,
						code_tyoe: state.code_type,
					});
					commit('M_IsAppEntry', true);
					commit('M_HAS_AUTH', true)
					resolve({
						...loginData
					})
				})
				.catch((e) => {
					Load.close()
					console.dir(e)
					Dialog
						.confirm({
							title: '提示',
							message: '链接超时, 是否重试?'
						})
						.then(() => {
							location.reload()
						})
						.catch(() => {
							window.HandleBack.closeWebView()
						});
					reject({})
					dispatch('setApiToken', '');
				})
		})
	}
}
export default {
	// namespaced: true,
	state,
	mutations,
	actions
}

/**
 * @typedef LoginDataProp
 * @property   {string} openid
 * @property   {string} userid
 * @property   {string} unionid
 * @property   {string} wx_headimgurl  微信头像
 * @property   {string} wx_nickname    微信昵称
 * @property   {boolean} marketing  是否为营销页
 * @property   {number|boolean} wxloginstate   (-1: 未注册)
 * @property   {0|1} [userrole]
 * @property   {boolean} [is_doctor]   是否为医生
 */

