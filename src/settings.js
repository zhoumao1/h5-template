module.exports = {
	title: '商城',
	baseRoute: 'insurances',
	/** @type {string<'doctor'|'patient'|'*'>}   权限列表, 用来控制 哪些身份的用户可进入*/
	permissionStr: 'patient'
}
