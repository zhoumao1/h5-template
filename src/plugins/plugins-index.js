import Load from 'vue-blur-load'
import WebView from './iframe/webView'
import RouterCache from './router-cache/index'
import './vant'
import './vconsole'
import './fastclick'
import VueRouteManager from 'vue-route-manager'
import VueWebPostmessage from './web-communication/index'

import { baseRoute } from '@/settings';
import wx from 'weixin-js-sdk'

export const installPlugin = (Vue, options = {}) => {
	Vue.use(Load)
	Vue.use(WebView)
	Vue.use(RouterCache, { router: options.router })
	Vue.use(VueRouteManager, { router: options.router })
	Vue.use(VueWebPostmessage, {
		base_route: baseRoute,
		wx: wx
	})
}
