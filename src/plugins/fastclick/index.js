/* eslint-disable no-undef */
// noinspection JSUnresolvedVariable

;(function () {
	let src = '//cdn.jsdelivr.net/npm/fastclick@1.0.6/lib/fastclick.min.js';
	// if (!/testTool=true/.test(window.location)) return;
	document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
	document.write('<scr' + 'ipt>  </scr' + 'ipt>');

	if ('addEventListener' in document) {
		document.addEventListener('DOMContentLoaded', function() {
			FastClick.attach(document.body);
			FastClick.prototype.focus = function (targetElement) {
				var length;
				//兼容处理:在iOS7中，有一些元素（如date、datetime、month等）在setSelectionRange会出现TypeError
				//这是因为这些元素并没有selectionStart和selectionEnd的整型数字属性，所以一旦引用就会报错，因此排除这些属性才使用setSelectionRange方法
				if (window.bridge.isIOS && targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time' && targetElement.type !== 'month' && targetElement.type !== 'email') {
					length = targetElement.value.length;
					targetElement.setSelectionRange(length, length);
					/*修复bug ios 11.3不弹出键盘，这里加上聚焦代码，让其强制聚焦弹出键盘*/
					targetElement.focus();
				} else {
					targetElement.focus();
				}
			};
		}, false);
	}
})();
