export default {
	componentName: 'router-cache',
	keyName: 'CACHEK',
	keyValueSrc: 'xxxxxxxx',
	pushName: 'push',
	goName: 'go',
	replaceName: 'replace',
	backName: 'back',
	forwardName: 'forward'
};
