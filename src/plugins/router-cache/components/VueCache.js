import history from '../history';
import config from '../config/config';
 

function isDef(v) {
	return v !== undefined && v !== null;
}

function isAsyncPlaceholder(node) {
	return node.isComment && node.asyncFactory;
}

function getFirstComponentChild(children) {
	if (Array.isArray(children)) {
		for (let i = 0; i < children.length; i++) {
			const c = children[i];
			if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
				return c;
			}
		}
	}
}
// 缓存堆栈
const cacheStack = [];

function getIndexByKey(key) {
	for (let index = 0; index < cacheStack.length; index++) {
		if (cacheStack[index].key === key) {
			return index;
		}
	}
	return -1;
}

let VueCache = keyName => {
	return {
		name: config.componentName,
		abstract: true,
		data() {
			return {};
		},
		props: {
			max: {
				type: [String, Number],
				default() {
					return '';
				}
			}
		},
		render() {
			let key = this.$route.query[keyName];
			const slot = this.$slots.default;
			//
			const vnode = getFirstComponentChild(slot);
			// console.log(vnode, 'vnode')
			if (!vnode) {
				return vnode;
			}
			let index = getIndexByKey(key);
			// console.log(index, key)
			// let cacheVNode = cacheStack[index].vnode
			// console.log(cacheVNode.key.split('?')[0], slot[0].key.split('?')[0])
			// isChildrenJump = cacheVNode.key.split('?')[0] !== vnode.key.split('?')[0]
			if (index !== - 1) {
				vnode.componentInstance = cacheStack[index].vnode.componentInstance;
				// destroy the instances that will be spliced
				for (let i = index + 1; i < cacheStack.length; i++) {
					cacheStack[i].vnode.componentInstance.$destroy();
					cacheStack[i] = null;
				}
				cacheStack.splice(index + 1);
				// 判断是否缓存了scrollTop
				const scrollTop = vnode.componentInstance.scrollTop;

				if (scrollTop) {

					// 如果缓存了就渲染，用定时器是因为无法判断dom何时渲染完成
					setTimeout(() => {
						vnode.elm.scrollTop = scrollTop;
					}, 100);
				}
				// console.log(cacheStack, key, vnode, '当前路由  存在堆栈中')
			} else {
				if (history.action === config.replaceName) {
					// destroy the instance
					cacheStack[cacheStack.length - 1].vnode.componentInstance.$destroy();
					cacheStack[cacheStack.length - 1] = null;
					cacheStack.splice(cacheStack.length - 1);
				}
				setTimeout(() => {
					// 监听dom的滚动事件，修改实例的滚动值
					vnode.elm.addEventListener("scroll", (e) => {
						vnode.componentInstance.scrollTop = e.target.scrollTop;
					});
				}, 100);
				cacheStack.push({ key, vnode });
				// console.log(cacheStack, key, vnode, '当前路由  不存在堆栈中')
			}
			vnode.data.keepAlive = true;
			return vnode;
		}
	};
};

function getStack() {
	return cacheStack;
}

export { VueCache, getIndexByKey, getStack };
