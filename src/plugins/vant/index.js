import {
	Icon,
	Tabs,
	Tab,
	NavBar,
	Toast,
	Popup,
	DatetimePicker,
	RadioGroup,
	Radio,
	Field,
	Button,
	Badge,
	Picker,
	ImagePreview,
	Dialog,
	Popover,
	Calendar,
	SwipeCell,
	Cell,
	CellGroup,
	Col,
	Row,
	ActionSheet,
	Checkbox,
	CheckboxGroup,
} from 'vant';

import Vue from 'vue'

import 'vant/lib/icon/style'
import 'vant/lib/tabs/style'
import 'vant/lib/nav-bar/style'
import 'vant/lib/toast/style'
import 'vant/lib/popup/style'
import 'vant/lib/datetime-picker/style'
import 'vant/lib/radio-group/style'
import 'vant/lib/radio/style'
import 'vant/lib/field/style'
import 'vant/lib/button/style'
import 'vant/lib/badge/style'
import 'vant/lib/picker/style'
import 'vant/lib/image-preview/style'
import 'vant/lib/dialog/style'
import 'vant/lib/popover/style'
import 'vant/lib/calendar/style'
import 'vant/lib/swipe-cell/style'
import 'vant/lib/dialog/style'
import 'vant/lib/cell/style'
import 'vant/lib/cell-group/style'
import 'vant/lib/col/style'
import 'vant/lib/row/style'
import 'vant/lib/action-sheet/style'
import 'vant/lib/checkbox/style'
import 'vant/lib/checkbox-group/style'

Vue.use(Icon)
Vue.use(Tabs)
Vue.use(Tab)
Vue.use(NavBar)
Vue.use(Toast)
Vue.use(Popup)
Vue.use(DatetimePicker)
Vue.use(RadioGroup)
Vue.use(Radio)
Vue.use(Field)
Vue.use(Button)
Vue.use(Badge)
Vue.use(Picker)
Vue.use(ImagePreview)
Vue.use(Popover)
Vue.use(Calendar)
Vue.use(SwipeCell)
Vue.use(Dialog)


Vue.use(Cell)
Vue.use(CellGroup)
Vue.use(Col)
Vue.use(Row)
Vue.use(ActionSheet);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);



Toast.setDefaultOptions({
	closeOnClick: true,
	closeOnClickOverlay: true
})

