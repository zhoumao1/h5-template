import Vue from 'vue'
import __components_webview from './components/webview'
import { afterLeave, deepMerge } from './utils';

const isServer = Vue.prototype.$isServer
const IframeConstructor = Vue.extend(__components_webview)

let defaultOptions = {
	url: ''
	// onClose: null,
	// onOpened: null,
	// duration: 2000,
};
let queue = []
let multiple = true

function createViewInstance() {
	/* istanbul ignore if */

	if (!queue.length || multiple) {
		let newIframe = new (Vue.extend(IframeConstructor))({
			el: document.createElement('div')
		});
		newIframe.$on('input', function (value) {
			newIframe.value = value;
		});
		queue.push(newIframe);
		// console.log(queue, '创建')
	}

	return queue[queue.length - 1];
}

const WebView = (options) => {
	if (options === void 0) {
		options = {};
	}
	let parent = options.el || document.body
	let viewInstance = createViewInstance()

	if (viewInstance.exist) {
		console.log('已存在')
		// l.updateZIndex();
	}

	options = deepMerge(defaultOptions, options)

	options.close = function () {
		viewInstance.exist = false;

		if (options.onClose) {
			options.onClose();
		}
		if (multiple && !isServer) {
			// eslint-disable-next-line no-unused-vars
			afterLeave(viewInstance, _ => {
				// const target = this.el || document.body
				removeTemplate(viewInstance)
				queue = queue.filter(function (item) {
					return item !== viewInstance;
				});
			}, 300);
			if (this) {
				this.show = false;
			} else
				viewInstance.show = false;
		}
	};

	Object.assign(viewInstance, options)
	parent.appendChild(viewInstance.$el)

	viewInstance.show = true;
	afterLeave(viewInstance, _ => {
		// const target = this.el || document.body
		viewInstance.url = options.url
	}, 300);

	return viewInstance;
}
WebView.close = function (all) {
	if (queue.length) {
		if (all) {
			queue.forEach(function (load) {
				load.close();
			});
			queue = [];
		} else if (!multiple) {
			queue[0].close();
		} else {
			queue.shift()
			.close();
			// console.log(queue.shift())
		}
	}
};
WebView.show = (url, is_jump_out) => {
	if (!url) throw '缺少 url'

	if(!url.includes('?') && !url.includes('ts=')){
		url = url+'?ts='+ (+new Date)
	}else if (url.includes('?') && !url.includes('ts=')){
		url = url+'&ts='+ (+new Date)
	}
	if(window.bridge.isIOS && /OS 12/i.test(window.bridge.ua) && is_jump_out){
		location.href = url
		return
	}
	return WebView({ url })
}

function removeTemplate(template) {
	if (template.$el && template.$el.parentNode) {
		template.$el.parentElement.style.position = ''
		template.$el.parentNode.removeChild(template.$el);
	}
	template.$destroy();
	// delete loadHash[key]
}

export default {
	install(Vue) {
		Vue.prototype.$WebView = WebView;
	},
	WebView
};

