import store from '@/store';
import DES from 'crypto-js/tripledes'
import Core from 'crypto-js/core'

const util = require("./util.js");
const md5 = require("md5");
const Version = "0.3.0";
var Secret = "83e1eab2376d4bb48bd871fc578532f7";
let CryptoInfo = {
	key: Core.enc.Utf8.parse('9fb35c36'),
	iv: Core.enc.Utf8.parse('722f9ec0')
}

export function makeSign(object, httpMethod, token) {
	var obj = {};
	if (httpMethod !== "POST") {
		obj = deepCopy(object);
	}

	obj.VERSION_INFO = Version;
	obj.TS = getCurrentDateTimeMillisecond();
	obj.TOKEN = token;
	obj.SECRET = Secret;


	if (httpMethod === "POST") {
		obj.POSTDATA = JSON.stringify(object);
	}
	var mapData = getObjProps(obj);
	// //console.log(mapData, "mapData");
	//得到签名字符串
	// console.log("sign:" + toQueryString(mapData));
	var sign = md5(toQueryString(mapData))
	.toUpperCase();
	// //console.log(sign);
	var filterData = filterOnlyRemainVersionAndTS(mapData);
	var queryString = toQueryString(filterData);

	// console.log(toQueryString(mapData), 'sign')
	return {
		sign: sign,
		queryString: queryString
	};
}

function deepCopy(p, c) {
	let a = c || {};
	for (var i in p) {
		// eslint-disable-next-line no-prototype-builtins
		if (!p.hasOwnProperty(i)) {
			continue;
		}
		if (typeof p[i] === "object") {
			a[i] = p[i].constructor === Array ? [] : {};
			deepCopy(p[i], a[i]);
		} else {
			a[i] = p[i];
		}
	}
	return a;
}

function getCurrentDateTimeMillisecond() {
	var timestamp = new Date().valueOf();
	return timestamp;
}

function getObjProps(obj) {
	var mapdata = [];
	let mapDataKey = [];
	let mapDataArry = [];
	for (var p in obj) {
		if (typeof obj[p] === "function") {
			continue;
		}
		// p 为属性名称，obj[p]为对应属性的值
		var item = {};
		item.Key = p.toUpperCase();
		item.Value = obj[p];
		mapDataKey.push(item.Key)
		mapdata.push(item);
	}

	mapDataKey.sort();
	mapDataKey.forEach((item, index) => {
		mapdata.forEach((element, num) => {
			if (item === element.Key) {
				mapDataArry.push(element)
			}
		})
	})

	return mapDataArry;
}

function toQueryString(mapData) {
	if (!mapData || mapData.length === 0) return "";
	var queryStr = "?";
	for (var item in mapData) {
		if (util.isNullOrEmpty(mapData[item].Value)) {
			continue;
		}
		queryStr += util.format("{0}={1}&", mapData[item].Key, mapData[item].Value);
	}
	return queryStr.slice(0, queryStr.length - 1);
}

function filterOnlyRemainVersionAndTS(mapData) {
	var filterArray = mapData.filter(function (item) {
		if (item.Key === "VERSION_INFO" || item.Key === "TS") {
			return true;
		}
		return false;
	});
	return filterArray;
}


export function encrypt(next) {
	var keyHex = CryptoInfo.key;
	var ivHex = CryptoInfo.iv;
	var encrypted = DES.encrypt(next, keyHex, {
			iv: ivHex,
			mode: Core.mode.CBC,
			padding: Core.pad.Pkcs7
		}
	);
	return encodeURIComponent(encrypted.toString());
}

export function decrypt(next) {
	var keyHex = CryptoInfo.key;
	var ivHex = CryptoInfo.iv;
	var decrypted = DES.decrypt(decodeURIComponent(next), keyHex, {
			iv: ivHex,
			mode: Core.mode.CBC,
			padding: Core.pad.Pkcs7
		}
	);
	// return decrypted.toString(Core.enc.Utf8)
	return Core.enc.Utf8.stringify(decrypted)
}

