export function isNullOrEmpty(str) {
  return str == null || str == undefined || str.length == 0;
}
export function format() {
  if (arguments.length == 0) return null;
  var str = arguments[0];
  for (var i = 1; i < arguments.length; i++) {
    var re = new RegExp("\\{" + (i - 1) + "\\}", "gm");
    str = str.replace(re, arguments[i]);
  }
  return str;
}
