/*
 axios全局配置
 包括全局URL，请求拦截，相应拦截
*/
import { monitorBug } from '@/houtai/monitorBug';

import axios from 'axios'
//import { options } from "@/libs/shijianyingshe.js";
import { makeSign } from '@/houtai/api.js'
// import { request } from "@/houtai/aliyun.js";
import store from "@/store";
import { Toast, Dialog } from 'vant';
import Load from 'vue-blur-load/src/load';
// import Load from 'vue-blur-load/src/load';
//url
// let axiosUrl = process.env.VUE_APP_URL;
// axios.defaults.baseURL = 'http://gate.shensx.com/eshop';
let requestPendingList = new Map()
/**
 * 添加请求
 * @param {Object} config
 */
const addPending = (config) => {
	config.cancelToken = config.cancelToken || new axios.CancelToken(cancel => {
		if (!requestPendingList.has(config.url)) { // 如果 requestPendingList 中不存在当前请求，则添加进去
			let exclude = ['SubmitDataBuringInfo', 'BatchGeneratePresignedUri']
			if (exclude.find(item => config.url.includes(item))) return
			requestPendingList.set(config.url, cancel)
		}
	})
}
/**
 * 移除请求
 * @param {Object} config
 */
const removePending = (config) => {
	if (requestPendingList.has(config.url)) { // 如果在 requestPendingList 中存在当前请求标识，需要取消当前请求，并且移除
		const cancel = requestPendingList.get(config.url)
		cancel(config.url)
		requestPendingList.delete(config.url)
	}
	// console.log(requestPendingList)
}

// http请求拦截器
axios.interceptors.request.use(
	function (config) {
		if (!('onUploadProgress' in config)) {
			config && removePending(config)
			config && addPending(config)
		}
		//console.log(config);
		//let aliyun = request(config);
		let token = store.state.apiAuth.apiToken || 'x';
		let result = makeSign(config.params || config.data,
			config.method.toUpperCase(), token);
		let sign = result.sign;

		// 超时
		config.timeout = 14000

		// 在发送请求之前做些什么
		config.url += result.queryString;
		config.headers = {
			SIGN: sign,
			TOKEN: token,
			pretty_json: 'on',
			'Cache-Control': 'no-cache'
			// "Content-MD5": aliyun.Md5,
			// "X-Ca-Nonce": aliyun.Nonce,
			// "X-Ca-Key": aliyun.AppKey,
			// "X-Ca-Signature": aliyun.Signature,
			// "X-Ca-SignatureMethod": "HmacSHA256",
			// "X-Ca-Signature-Headers": aliyun.SignatureHeaders
		};
		process.env.VUE_APP_IS_LOCAL ? config.headers.authorization = 'Bearer '+ token:''
		//console.log(config);
		return config
	},
	function (error) {
		// 对请求错误做些什么
		monitorBug(error)
		return Promise.reject(error)
	}
);

// http响应拦截器
axios.interceptors.response.use((res) => {
	// 递归的将字典的key转为小写

	let trans = function (data) {
		if (data.constructor === Object) {
			for (let i in data) {
				if (data[i] === null) {
					delete data[i]
				} else {
					data[i.toLowerCase()] = trans(data[i]);
				}
			}
		} else if (data.constructor === Array) {
			for (let i in data) {
				if (data[i] === null) {
					delete data[i]
				} else {
					data[i] = trans(data[i]);
				}
			}
		}

		return data;
	};

	res.data = trans(res.data);

	return res;
}, function (error) {
	monitorBug(error)
	console.dir(error)
	if(error.message){
		Load.close()
	}
	error.config && removePending(error.config)
	Load.close(true)
	if(error.config.url.includes('MallApi/TrackEvent')){
		return Promise.reject(error.response)
	}
	if (error.response === undefined && error.code === 'ECONNABORTED') {
		Toast('连接超时')
	}
	if (error.response === undefined) return
	if (error.response.status === 410) {
		let flag = error.response.data.errorinfo || error.response.data.ERRORINFO
		if (flag && flag.length) {
			Toast.fail(error.response.data.errorinfo || error.response.data.ERRORINFO);
		}
	} else {
		Toast('请求出错')
	}
	return Promise.reject(error.response)
});

export default axios;
