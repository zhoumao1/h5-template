import axios from '@/api/index.js'

// 静默注册
// https://www.showdoc.cc/kidneytec?page_id=2477013183943431
export function SilentBindOrRegisterForH5(data) {
  return axios({
    method: 'post',
    url: '/api/WxH5Api/SilentBindOrRegisterForH5',
    data: { data: data }
  })
}
// 发送注册手机验证码
// http://doc.shensx.com/project/12/interface/api/1378
export function GetBindUserTelVCodeForH5(data) {
  return axios({
    method: 'get',
    url: '/api/WxH5Api/GetBindUserTelVCodeForH5',
    params: data
  })
}

//上传阿里云图库
export function GetOSSPolicyAndSignForMiniApp() {
	return axios({
		method: 'get',
		url: '/api/MiniApp/GetOSSPolicyAndSignForMiniApp',
		params: {}
	})
}
//获取阿里云图库
export function GeneratePresignedUri(data) {
	return axios({
		method: 'post',
		url: '/api/WXH5Api/BatchGeneratePresignedUri',
		data: { data: data }
	})
}
// 提交数据埋点信息
// https://www.showdoc.cc/kidneytec?page_id=1803500111624312
export function SubmitDataBuringInfo(data) {
	return axios({
		method: 'post',
		url: '/api/userinfoAPI/SubmitDataBuringInfo',
		data: { data }
	})
}
