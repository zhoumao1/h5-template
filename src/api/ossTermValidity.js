import axios from '@/api/index'

import { GetOSSPolicyAndSignForMiniApp } from '@/api/public.js';
import { Toast } from 'vant';
let instance = axios.create();
let md5 = require("md5");
export async function termValidity(file) {
  if (file === null) return Toast('图片信息获取失败')
  let date = (new Date()).valueOf();
  let data = JSON.parse(sessionStorage.getItem('oss'))
  if (data === null || parseInt(date / 1000) > parseInt(data.expire)) {
    let dat = await GetOSSPolicyAndSignForMiniApp()
    console.log(dat, 'sss')
    sessionStorage.setItem('oss', JSON.stringify(dat.data))
    if (file !== null) {
      return publicRequest(file).then((result) => {
        return result.data
      }).catch(() => {
        return null
      });
    }
  } else {
    return publicRequest(file).then((result) => {
      return result.data
    }).catch(() => {
      return null
    });
  }
}

function publicRequest(record) {
  let data = JSON.parse(sessionStorage.getItem('oss'));

  let item = dataURLtoBlob(record);
  let formData = new FormData();
  formData.append('key', data.dir + md5(record)); //签名
  formData.append('policy', data.policy); //签名
  formData.append('OSSAccessKeyId', data.accessid); //签名
  formData.append('Signature', data.signature); //签名
  formData.append('file', item); //签名
  formData.append('success_action_status', 200); //签名



  instance.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    response.data = formData.get("key")

    console.log(response, 'response')
    return response
  }, function () {
    // 对响应错误做点什么
    return null
  });

  return instance({
    method: 'POST',
    url: data.host,
    data: formData
  }
  )
}

let dataURLtoBlob = (dataurl) => {
  let arr = dataurl.split(','); let mime = arr[0].match(/:(.*?);/)[1];
  let bstr = atob(arr[1]); let n = bstr.length; let u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], { type: mime });
};






