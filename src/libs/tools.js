/**
 * 绑定 after-leave 事件在 Vue 的子实例中, 确保动画结束后可以执行
 *
 * @param {Vue} instance Vue instance.
 * @param {Function} callback callback of after-leave event
 * @param options    { { event: string, speed: number, once: boolean } }
 * @property options.event 过渡结束后触发的事件名
 * @property options.speed 过渡速度，默认值为220ms
 * @property options.once  是否只绑定一次事件. 默认值为false。
 */
export const afterLeave = (instance, callback, options = { event: '', speed: 220, once: false }) => {
	if(!instance || !callback) throw new Error('instance & callback is required');
	let called = false;
	const afterLeaveCallback = function () {
		if(called) return;
		called = true;
		if(callback) {
			callback.apply(null, arguments);
		}
	};
	if(options.event){
		if (options.once) {
			instance.$once(options.event, afterLeaveCallback);
		} else {
			instance.$on(options.event, afterLeaveCallback);
		}
	}else {
		if (options.once) {
			instance.$once('after-leave', afterLeaveCallback);
		} else {
			instance.$on('after-leave', afterLeaveCallback);
		}
		setTimeout(() => {
			afterLeaveCallback();
		}, options.speed + 50);
	}
};

/**
 *
 * @param url
 * @return {{}}
 */
export const param2Obj = (url = location.search) => {
	const s = decodeURIComponent(url.split('?')[1])
		.replace(/\+/g, ' ')
	if (!s) {
		return {}
	}
	const obj = {}
	const sArr = s.split('&')
	sArr.forEach(item => {
		const i = item.indexOf('=')
		if (i !== -1) {
			const k = item.substring(0, i)
			const v = item.substring(i + 1, item.length)
			obj[k] = v
		}
	})
	return obj
}
