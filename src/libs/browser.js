/*
判断是否是微信浏览器
*/
import { Dialog } from 'vant';
import wx from 'weixin-js-sdk';
import './interaction'
import { baseRoute } from '@/settings';
import config from '@/plugins/router-cache/config/config';

export function wechatFlag() {
	var ua = window.navigator.userAgent.toLowerCase();
	return {
		// 是否为微信
		is_wechat: ua.indexOf('micromessenger') > - 1,
		// 是否为微信开发者工具
		is_wechat_devtools: ua.includes('wechatdevtools'),
		is_mini_program: window.__wxjs_environment === 'miniprogram'
	}
}


/**
 * 处理浏览器回退事件
 */
export class BackHandle {

	/**
	 * @param href {string=}   指定地址添加状态
	 */
	constructor(href) {
		this.href = href || location.href
	}

	/**
	 * 添加 记录
	 * @param state      {{ flag: string}}    该条记录的标识符
	 * @param eventListener   {function=}  需要监听的事件
	 */
	push(state, eventListener) {
		if(!state.flag) throw new RangeError('缺少必要的属性: flag' + this.href);
		history.pushState(state, document.title, this.href);
		eventListener && window.addEventListener('popstate', eventListener, true)
	}

	/**
	 * 删除 记录
	 * @param state      {{ flag: string}}    上条记录的标识符
	 * @param eventListener   {function=}  需要移除监听的事件
	 */
	remove(state, eventListener) {
		if(!state.flag) throw new RangeError('缺少必要的属性: flag');
		if(Object.values(history.state)
			.includes(state.flag)) {
			history.go(- 1)
			eventListener && window.removeEventListener('popstate', eventListener, true)
		}
	}

	/**
	 * 关闭 h5 页面
	 * @param is_need_prompt   {boolean=true}   是否需要提示
	 * @param state   {{ flag: 'close-page' }=} 上条记录的标识符
	 * @param befor_cancel     {function=}        取消前的回调
	 */
	closeH5Page(is_need_prompt = false, state = { flag: 'close-page' }, befor_cancel) {

		if(is_need_prompt) {
			if(!state && !state.flag) throw '缺少必要参数 state, state = { flag: "标识str" }'
			this.push(state)

			Dialog.confirm({
				message: `是否关闭【${ document.title }】`,
				confirmButtonText: '关闭',
				confirmButtonColor: '#EB8E45',
				cancelButtonText: '取消'
			})
				.then(() => {
					wx.closeWindow()
				})
				.catch(() => {
				});
		} else {
			wx.closeWindow()
		}
	}

	// 关闭 webView
	closeWebView() {
		// '{"APP_EVENT":"DoFinish","ENABLE":1,"INTERACTIVE_TYPE":2,"STYLE":0,"TEXT":"关闭"}'
		const CLOSEDATA = { "APP_EVENT": "DoFinish", "ENABLE": 1, "INTERACTIVE_TYPE": 2, "STYLE": 0, "TEXT": "关闭" }
		window.bridge.callHandler({}, CLOSEDATA)
		// baseEntity.jsCallClient('{"APP_EVENT":"DoFinish","ENABLE":1,"INTERACTIVE_TYPE":2,"STYLE":0,"TEXT":"关闭"}');
	}
}

/**
 * 处理安卓浏览器回退
 */
export class AndroidBack {
	constructor() {
		// 是否刷新页面了
		this.isRefresh = false
		window.bridge.isAndroid && this.init()
		this.STATE = baseRoute + '__his_state'
	}

	makeState(his_state) {
		let hisStateArr = []
		if(!sessionStorage.getItem(this.STATE)) {
			sessionStorage.setItem(this.STATE, JSON.stringify([his_state]))
		} else {
			hisStateArr = JSON.parse(sessionStorage.getItem(this.STATE))
			console.log(hisStateArr, 1)
			hisStateArr.push(his_state)
			sessionStorage.setItem(this.STATE, JSON.stringify(hisStateArr))
		}
	}

	removeState() {
		sessionStorage.removeItem(this.STATE)
	}

	init() {
		window.addEventListener('beforeunload', () =>{
				alert(JSON.stringify({ state: history.state, r: this.isRefresh }))
			// if(history.state && history.state.flag){
			// 	this.isRefresh = true
			// }


		})
		window.addEventListener('pageshow', (e) => {
			// if(this.isRefresh)return
			this.makeState(history.state);
			//0: 正常进入, 1: 刷新, 2: 后退
			if(window.performance && window.performance.navigation.type === 0)return
			//https://developer.mozilla.org/zh-CN/docs/Web/API/PerformanceNavigationTiming
			let perfEntries = performance.getEntriesByType("navigation");
			let entry = perfEntries[0];
			let performanceJson = entry.toJSON();
			if(performanceJson.type === 'navigate' || performanceJson.type === 'reload')return
			let tmp = JSON.parse(sessionStorage.getItem(this.STATE))
			if(tmp && tmp.length >= 3) {
				let backHandle = new BackHandle()
				backHandle.push({ flag: 'back-refresh' })

				let flag = confirm('是否退出?')
				if(flag) {
					backHandle.closeH5Page()
				} else {
					this.removeState()
					// location.replace(location.href)
					// router.replace({
					// 	path: store.getters.homeRoute.path
					// })
					history.back()
				}
			}

		})
	}

}




class MiniProgramRouter {

	constructor() {
		this.wx = wx.miniProgram
	}

	static getInstance() {
		if (!MiniProgramRouter.instance) {
			MiniProgramRouter.instance = new MiniProgramRouter();
		}
		return MiniProgramRouter.instance;
	}

	_getQueryStr(query) {
		if (!query) return ''
		return Object.keys(query)
			.map(k => `${k}=${query[k]}`)
			.join('&');
	}

	/**
	 * 跳转
	 * @param {Object}   route       路由信息
	 * @param {string}   route.path        路径
	 * @param {Object}   [route.query]   成功后返回事件
	 * @param {''|'tab'} [type]      跳转类型(跳转到 tab 需要)
	 * @see wx.navigateTo
	 */
	push(route, type = '') {
		let { path, query } = route
		if (type === 'tab') {
			this.wx.switchTab({
				url: path
			})
			return
		}
		console.log(route, 'route')
		console.log(`${path}?${this._getQueryStr(query)}`)
		this.wx.navigateTo({
			url: `${path}?${this._getQueryStr(query)}`,
			fail(e) {
				console.error(e, 'navigateTo 错误')
			}
		})
	}

	/**
	 * 返回上一栈, 或 指定栈
	 * @param delta   {number=} 返回的页面数，如果 delta 大于现有页面数，则返回到首页
	 * @see WechatMiniprogram.Wx.navigateBack
	 */
	back(delta) {
		this.wx.navigateBack({
			delta,
			fail(e) {
				console.error(e, 'navigateBack 错误')
			}
		})
	}

	/**
	 * 重定向
	 * @param path    {string}    路径
	 * @param query   {Object=}    参数
	 * @see wx.redirectTo
	 */
	replace({ path, query }) {
		this.wx.redirectTo({
			url: `${path}?${this._getQueryStr(query)}`,
			fail(e) {
				console.error(e, 'redirectTo 错误')
			}
		})
	}
}

export const miniProgramRouter = MiniProgramRouter.getInstance();

/**
 * 处理浏览器回退事件
 */
class BackHandle_2 {

	/**
	 * @param href {string=}   指定地址添加状态
	 */
	constructor(href) {
		this.backStacks = []
		this.href = href || location.href
	}

	static getInstance(href) {
		if (!BackHandle_2.instance) {
			BackHandle_2.instance = new BackHandle_2(href);
		}
		return BackHandle_2.instance;
	}

	/**
	 * 更新地址 url
	 * @param [href]
	 */
	updateHref(href = location.href) {
		this.href = href
	}

	handlePopState() {
		let handle = BackHandle_2.instance.backStacks.pop()
		// console.log(handle, BackHandle_2.instance.#backStacks)
		if (handle) return handle
	}

	/**
	 * 添加 记录
	 * @param state      {{ flag: string}}    该条记录的标识符
	 * @param eventListener   {function=}  需要监听的事件
	 */
	push(state, eventListener) {
		if (!state.flag) throw new RangeError('缺少必要的属性: flag' + this.href);
		this.updateHref()
		history.pushState(state, document.title, this.href);
		if (!eventListener) return
		this.backStacks.push(eventListener)
		window.addEventListener('popstate', eventListener, false)
	}

	/**
	 * 删除 记录
	 * @param state      {{ flag: string}}    上条记录的标识符
	 * @param eventListener   {function=}  需要移除监听的事件
	 */
	remove(state, eventListener) {
		if (!state.flag) throw new RangeError('缺少必要的属性: flag');
		if (Object.values(history.state)
			.includes(state.flag)) {
			let fun = this.handlePopState()
			window.removeEventListener('popstate', fun, false)
			history.go(-1)
		}
	}

	/**
	 * 关闭 h5 页面
	 * @param is_need_prompt   {boolean=true}   是否需要提示
	 * @param state   {{ flag: 'close-page' }=} 上条记录的标识符
	 * @param befor_cancel     {function=}        取消前的回调
	 */
	closeH5Page(is_need_prompt = false, state = { flag: 'close-page' }, befor_cancel) {

		if (is_need_prompt) {
			if (!state && !state.flag) throw '缺少必要参数 state, state = { flag: "标识str" }'
			this.push(state)

			Dialog.confirm({
					message: `是否关闭【${document.title}】`,
					confirmButtonText: '关闭',
					confirmButtonColor: '#EB8E45',
					cancelButtonText: '取消'
				})
				.then(() => {
					if (wechatFlag().is_mini_program) {
						miniProgramRouter.back()
					} else {
						wx.closeWindow()
					}
				})
				.catch(() => {
				});
		} else {
			wx.closeWindow()
		}
	}

	// 关闭 webView
	closeWebView() {
		// '{"APP_EVENT":"DoFinish","ENABLE":1,"INTERACTIVE_TYPE":2,"STYLE":0,"TEXT":"关闭"}'
		const CLOSEDATA = { "APP_EVENT": "DoFinish", "ENABLE": 1, "INTERACTIVE_TYPE": 2, "STYLE": 0, "TEXT": "关闭" }
		window.bridge.callHandler({}, CLOSEDATA)
		// baseEntity.jsCallClient('{"APP_EVENT":"DoFinish","ENABLE":1,"INTERACTIVE_TYPE":2,"STYLE":0,"TEXT":"关闭"}');
	}
}

/**@type {BackHandle_2} 返回处理 */
window.HandleBack = BackHandle_2.getInstance()

/**
 * 获取当前网址url 不包括 缓存字段
 * @return {string}
 */
export function getLocationHref(){
	//统一设置分享
	let url = location.href
	let r = new RegExp(`\\??${config.keyName}=.{${config.keyValueSrc.length}}&?`)
	return url.split("#")[0].replace(r, '');
}

