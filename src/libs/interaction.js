
function Bridge() {
	this.ua = navigator.userAgent;
	// eslint-disable-next-line no-useless-escape
	this.isAndroid = (/(Android);?[\s\/]+([\d.]+)?/.test(this.ua));
	this.isIOS = !!this.ua.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
	this.isH5 = this.ua.toLowerCase().indexOf('micromessenger') > - 1

	this.msgCallbackMap = {};
	this.getNextCallbackID = function () {
		var outTradeNo = "";
		for (var i = 0; i < 6; i ++) {
			outTradeNo += Math.floor(Math.random() * 10);
		}
		outTradeNo = new Date().getTime() + outTradeNo;
		return outTradeNo
	};
	/**
	 * JS调用Native
	 * @param {Object}   action         事件名【可以传空对象, 但是不能不传】
	 * @param {string}  		[action.ios = 'jsclick']     IOS事件名
	 * @param {string}   	[action.android = 'jsCallClient']    Android事件名
	 * @param {string|Array}   	[action.h5 = '*']      H5 分发的域名或域名列表
	 * @param {Object}   [params]       交互数据
	 * @param {Object}   [cb_info]       回调信息
	 * @param {string}   [cb_info.name]     回调名(安卓用)
	 * @param {function} 	[cb_info.func]     交互回调
	 */
	this.callHandler = function (action, params, cb_info) {
		!action.ios && (action.ios = 'jsclick')
		!action.h5 && (action.h5 = '*')
		!action.android && (action.android = 'jsCallClient')
		!cb_info && (cb_info = {})
		// console.log(this.isH5, action.h5, 'link')
		/*function receiveMessage(event)
		{
			// 我们能信任信息来源吗？
			// if (event.origin !== "http://example.com:8080")
			// 	if (event.origin !== "http://192.168.1.121:8082/")
			// 	return;

			// event.source 就当前弹出页的来源页面
			// event.data 是 "hello there!"
			console.log('接受到的信息是：' + event.data)

			// 假设你已经验证了所受到信息的origin (任何时候你都应该这样做), 一个很方便的方式就是把event.source
			// 作为回信的对象，并且把event.origin作为targetOrigin
			// event.source.postMessage("hi there yourself!  the secret response " +
			// 	"is: rheeeeet!",
			// 	event.origin);
		}

		window.addEventListener("message", receiveMessage, false);*/
		if(this.isH5 && action.h5){
			// console.log(action, params, '点击反馈')0/');
			if(typeof action.h5 === 'string'){
				window.parent.postMessage(JSON.stringify(params), action.h5);
			}else {
				action.h5.forEach(item => {
					window.parent.postMessage(JSON.stringify(params), item);
				})
			}
			return
		}

		var data = { action: '' };

		data.params = params || ''
		if(cb_info.func && typeof (cb_info.func) === 'function') {
			var callbackid = this.getNextCallbackID();
			this.msgCallbackMap[callbackid] = cb_info.func;
			data.callbackId = callbackid;
			data.callbackFunction = 'window.bridge.callbackDispatcher';
		}

		if(this.isIOS) {
			data.action = action.ios
			data.params = params || {}
			// console.log(data)
			try {
				window.webkit['messageHandlers']['nativeObject'].postMessage(data);
			} catch (error) {
				show_debug_info('error native message');
			}
		}

		if(this.isAndroid) {
			data.action = action.android
			try {
				if(cb_info.func) {
					/**@typedef {Object} baseEntity*/
					// eslint-disable-next-line no-undef
					baseEntity[data.action](JSON.stringify(data.params), cb_info.name);
				} else {
					// console.log(action, JSON.stringify(data.params), 111111)
					// eslint-disable-next-line no-undef
					baseEntity[data.action](JSON.stringify(data.params));
					// baseEntity.jsCallClient(JSON.stringify(data.params));
				}
				// android_bridge.receivedMessage(action, params, data.callbackId, data.callbackFunction);
				// baseEntity.jsCallClient(params)
			} catch (error) {
				show_debug_info('error native message');
			}
		}
	};

	// JS成功调用Native后，Native的回调
	this.callbackDispatcher = function (callbackId, resultjson) {
		var handler = this.msgCallbackMap[callbackId];
		if(handler && typeof (handler) === 'function') {
			show_debug_info(resultjson);
			var resultObj = resultjson ? JSON.parse(resultjson) : {};
			handler(resultObj);
			if(resultObj["__processing__"] === undefined) {
				delete this.msgCallbackMap[callbackId]
			}
		}
	};
	this.nativeEventDispatcher = function (eventId, resultJson) {
		var handlerArr = this.eventCallMap[eventId];
		for (var key in handlerArr) {
			// eslint-disable-next-line no-prototype-builtins
			if(handlerArr.hasOwnProperty(key)) {
				var handler = handlerArr[key];
				if(handler && typeof (handler) === 'function') {
					var resultObj = resultJson ? JSON.parse(resultJson) : {};
					handler(resultObj);
				}
			}
		}
	};

	/**
	 * 下面的一个字典和三个函数用于Native调用JS的场景支持，完整流程如下：
	 *    1. JS调用registerHandler注册一个函数供Native调用，本质是以eventId为键存入eventCallMap；
	 *    2. Native端发来调用，进入nativeEventDispatcher，本质是检索eventCallMap找到对应的处理函数，并填入参数；
	 *    3. registerHandler注册的函数实现中可通过调用callbackNative反馈信息给Native端
	 */
	this.eventCallMap = {};
	// 监听Native的调用事件，handler的类型为 (请求参数字典) -> Void，供JS调用
	this.registerHandler = function (eventId, handler) {
		var handlerArr = this.eventCallMap[eventId];
		if(handlerArr === undefined) {
			handlerArr = [];
			this.eventCallMap[eventId] = handlerArr;
		}
		if(handler !== undefined) {
			handlerArr.push(handler);
		}
	};

	// JS处理Native的调用请求过程中，可通过此函数回调Native，以告知Native处理进度或处理结果，供JS调用
	this.callbackNative = function (requestParams, resultJson, processing) {
		// eslint-disable-next-line no-redeclare
		var resultJson = arguments[1] ? arguments[1] : {}
		// eslint-disable-next-line no-redeclare
		var processing = arguments[2] ? arguments[2] : false

		if(requestParams["callbackId"] !== undefined) {
			var responseParams = { "callbackId": requestParams["callbackId"], "parameters": resultJson }
			if(processing) {
				responseParams["__processing__"] = true
			}
			this.callHandler("__callback_native__", responseParams, null)
		}
	}
};
window.bridge = new Bridge();

// 以下都是示例代码
// JS调用Native
// window.document.getElementById("button_div").onclick = function () {
// 	show_debug_info('尝试调用');
// 	window.bridge.callHandler("jsclick",
// 		{ "param01": 1, "param02": "string2" },
// 		function (result) {
// 			show_debug_info(result["tip"]);
// 		})
// }

// Native调用JS
window.bridge.registerHandler("calljs", function (p) {
	show_debug_info('js正在处理来自Native的调用, 参数:' + p)
	var tick = 0;
	setTimeout(function run() {
		tick += 1;
		if(tick < 100) {
			window.bridge.callbackNative(p, { "p4": "string4", "tip": tick }, true);
			show_debug_info('js正在处理来自Native的调用: ' + tick + '%');
			setTimeout(run, 100);
		} else {
			window.bridge.callbackNative(p, { "p4": "string4" });
			show_debug_info('js已处理完来自Native的调用')
		}
	}, 100);
})

function show_debug_info(info) {
	console.log(info);
	// window.document.getElementById("debug_info").innerHTML = info;
}
