import wx from 'weixin-js-sdk';
import { GenerateShareFriendSignInfo, SubmitWeChatPayForh5 } from "@/api/wx.js";

/**
 * 微信分享
 * @param {Object}   share_config   分享配置
 * @param   {string} share_config.title   标题
 * @param   {string} share_config.desc    描述文案
 * @param   {string} share_config.link    分享链接
 * @param   {string} share_config.img     缩略图
 */
export function getShareSDK(share_config) {
	let params = { path: location.href.split("#")[0] };
	//console.log(params, 'GenerateShareFriendSignInfo param');
	GenerateShareFriendSignInfo(params)
		.then(res => {
			console.log(params, share_config)
			console.log(res, 'GenerateShareFriendSignInfo');
			let resData = res.data;
			wx.config({
				// debug: location.href.includes('testTool=true'),
				debug: false,
				appId: resData.appid, // 必填，公众号的唯一标识
				timestamp: resData.timestamp, // 必填，生成签名的时间戳
				nonceStr: resData.noncestr, // 必填，生成签名的随机串
				signature: resData.signature, // 必填，签名
				jsApiList: ['updateTimelineShareData', 'updateAppMessageShareData', 'hideAllNonBaseMenuItem', 'showMenuItems', 'hideMenuItems'] // 必填，
			});

			wx.ready(function () {
				wx.hideAllNonBaseMenuItem();
				wx.hideMenuItems({
					menuList: [
						// "menuItem:share:appMessage",
						// "menuItem:share:timeline",
						// "menuItem:share:qq",
						// "menuItem:share:weiboApp",
						// "menuItem:share:facebook",
						// "menuItem:share:QZone",
						"menuItem:copyUrl",
						"menuItem:openWithQQBrowser",
						"menuItem:openWithSafari",
						"menuItem:readMode",
						"menuItem:share:email",
						"menuItem:editTag",
						"menuItem:originPage",
						"menuItem:share:brand"
					],
					// 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮，所有menu项见附录3
					success: function (res) {
						console.log(res, "隐藏成功");
					},
					fail: function (res) {
						console.log(res, "隐藏失败");
					}
				});
				wx.showMenuItems({
					menuList: [
						"menuItem:share:appMessage",
						"menuItem:share:timeline",
						"menuItem:favorite"
					], // 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮，所有menu项见附录3
					success: function (res) {

					},
					fail: function (res) {

					}
				});
				// 分享到对话消息
				wx.updateAppMessageShareData({
					title: share_config.title, // 分享标题
					desc: share_config.desc, // 分享描述
					link: share_config.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
					imgUrl: share_config.img, // 分享图标
					trigger: function trigger(res) {
					},
					success: function success(res) {
						//console.log('已分享');
					},
					cancel: function cancel(res) {
						//console.log('已取消分享');
					},
					fail: function fail(res) {
						//console.log('分享失败');
					}
				});

				// 分享到朋友圈
				wx.updateTimelineShareData({
					title: share_config.title, // 分享标题
					desc: share_config.desc, // 分享描述
					link: share_config.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
					imgUrl: share_config.img, // 分享图标
					trigger: function trigger(res) {
					},
					success: function success(res) {
						//console.log('已分享');
					},
					cancel: function cancel(res) {
						//console.log('已取消分享');
					},
					fail: function fail(res) {
						//console.log('分享失败');
					}
				});
			});

			wx.error(function (res) {
				//alert("微信验证失败");
			});
		})
}

/*
微信支付方法
@param{orderid}: 订单号
@param{successCallback}: 成功回调
@param{failCallback}: 失败回调
*/
export async function wxPay(data, successCallback, failCallback) {
	try {
		let signParams = { path: window.location.href.split('#')[0] };
		const signRes = await GenerateShareFriendSignInfo(signParams);
		let signData = signRes.data;

		// let payParams = {
		// 	unionid: store.state.wx.unionid,
		// 	openid: store.state.wx.openid,
		// 	orderid: orderid,
		// 	token: store.state.apiAuth.apiToken,
		// 	MiniAppType: 6
		// };
		//
		// const payRes = await SubmitWeChatPayForh5(payParams);
		//
		let payData = data['payparam'];

		console.log(payData, window.location.href.split('#')[0], 'payData')
		wx.config({
			debug: false,
			appId: payData.appid, // 必填，公众号的唯一标识
			timestamp: payData.timestamp, // 必填，生成签名的时间戳
			nonceStr: payData.noncestr, // 必填，生成签名的随机串
			signature: signData.signature, // 必填，签名
			jsApiList: ['chooseWXPay'] // 必填，需要使用的JS接口列表
		});

		wx.ready(function () {
			wx.chooseWXPay({
				appId: payData.appid,
				timestamp: payData.timestamp,
				nonceStr: payData.noncestr,
				package: payData['package']/*payData.package*/,
				signType: 'MD5',
				paySign: payData['paysign'],
				success(res) {
					//console.log('支付成功');
					if (successCallback) {
						successCallback(res);
					}
				},
				fail(res) {
					console.log('支付失败')
					if (failCallback) {
						failCallback(res);
					}
				},
				cancel(res) {
					if (failCallback) {
						failCallback(res);
					}
				}
			});
		});
		wx.error(function (res) {
			//console.log(res);
			if (failCallback) {
				failCallback(res);
			}
		});
	} catch (e) {
		if (failCallback) {
			failCallback(e);
		}
	}
}

export async function noSharing(){
	let { data } = await GenerateShareFriendSignInfo({
		path: location.href.split("#")[0]
	})

	wx.config({
		// debug: location.href.includes('testTool=true'),
		debug: false,
		appId: data.appid, // 必填，公众号的唯一标识
		timestamp: data.timestamp, // 必填，生成签名的时间戳
		nonceStr: data.noncestr, // 必填，生成签名的随机串
		signature: data.signature, // 必填，签名
		jsApiList: ['hideAllNonBaseMenuItem', 'hideMenuItems'] // 必填，
	});

	wx.ready(() =>{
		wx.hideMenuItems({
			menuList: [
				"menuItem:share:appMessage",
				"menuItem:share:timeline",
				"menuItem:share:qq",
				"menuItem:share:weiboApp",
				"menuItem:share:facebook",
				"menuItem:share:QZone",
				"menuItem:copyUrl",
				"menuItem:openWithQQBrowser",
				"menuItem:openWithSafari",
				"menuItem:readMode",
				"menuItem:share:email",
				"menuItem:editTag",
				"menuItem:originPage",
				"menuItem:share:brand"
			],
			// 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮，所有menu项见附录3
			success: function (res) {
				console.log(res, "隐藏成功");
			},
			fail: function (res) {
				console.log(res, "隐藏失败");
			}
		});
	})
}

export function videoShareWx(url, obj) {
	//自定义分享
	console.log("分享-开始微信签名-------");
	console.log("执行路径：", window.location.href);
	console.log("签名路径：", url);
	console.log("obj", obj);
	getShareSDK(url, obj);
}
