module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "eslint:recommended", "@vue/prettier"],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "off" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    'quotes': 'off',
    "space-before-function-paren": "off",
    "semi": "off",
    "no-empty": "off",
    "no-tabs": "off",
    "spaced-comment": "off",
    "no-unused-vars": "off",
    "prettier/prettier": "off",
    "no-inner-declarations": "off",
  }
};
