'use strict';
const path = require("path");
const { baseRoute } = require('./src/settings');
const FileManagerPlugin = require('filemanager-webpack-plugin');
const GhostProgressWebpackPlugin = require('ghost-progress-webpack-plugin')
const HappyPack = require('happypack')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin')
const IS_BUILD_MODE = process.env.VUE_APP_RUN_MODE.includes('build')
const outputDirName = (IS_BUILD_MODE ? process.env.VUE_APP_RUN_MODE.substr(6) : process.env.VUE_APP_RUN_MODE)
const resolve = dir => {
	return path.join(__dirname, dir);
};
const BASE_URL = "/";
console.log('origin: ~', process.env.VUE_APP_URL)
console.log('运行模式: ~', process.env.VUE_APP_RUN_MODE)
module.exports = {
	publicPath: BASE_URL,
	assetsDir: baseRoute,
	// 兼容低版本手机
	transpileDependencies: ['webpack-dev-server/client'],
	chainWebpack: config => {
		config.plugin('moment-locales-webpack-plugin')
			.use(MomentLocalesPlugin)
			.tap(args =>{
				args[0] ={
					localesToKeep: ['zh-cn'],
				}
				return args
			})

		config.resolve.alias
		.set("@", resolve("src")) // key,value自行定义，比如.set('@@', resolve('src/components'))
		.set("_c", resolve("src/components"))
		.set("_v", resolve("src/views"));

		// 兼容低版本手机
		config.module.rule('compile')
			.test(/\.js$/)
			.include
			.add(resolve('src'))
			.add(resolve('test'))
			.add(resolve('node_modules/webpack-dev-server/client'))
			.add(resolve('node_modules'))
			.end()
			.use('babel')
			.loader('babel-loader')
			.options({
				presets: [
					['@babel/preset-env', {
						modules: false
					}]
				]
			})
		if(!process.env.VUE_APP_RUN_MODE.includes('build'))return
		config.plugin('filemanager-webpack-plugin')
			.use(FileManagerPlugin)
			.tap((c) =>{
				c[0] = {
					events: {
						onEnd: {
							// delete: [`./dist/${outputDirName}.zip`/*, `./dist/${outputDirName}/`*/],
							// archive: [{ source: "./dist/"+outputDirName, destination: `./dist/${outputDirName}.zip` }]
							delete: [`./${baseRoute}.zip`/*, `./dist/${outputDirName}/`*/],
							archive: [{ source: "./dist/"+outputDirName, destination: `./${baseRoute}.zip` }]
						}
					}
				}
				return c;
			})
		// 进度
		config
			.plugin('ghost-progress-webpack-plugin')
			.use(GhostProgressWebpackPlugin.GhostProgressPlugin('detailed'))

		// 多线程
		config.plugin('HappyPack').use(HappyPack, [
			{
				loaders: [
					{
						loader: 'babel-loader?cacheDirectory=true'
					}
				]
			}
		])
	},
	configureWebpack: {
		devtool: "source-map"
	},
	productionSourceMap: false,
	lintOnSave: false,
	css: {
		loaderOptions: {
			postcss: {
				plugins: [
					require("postcss-px2rem")({ remUnit: 37.5 }) // 换算的基数
				]
			}
		}
	},
	outputDir: "dist/" + outputDirName,
	devServer: {
		proxy: {
			'/api': {    // search为转发路径
				target: process.env.VUE_APP_URL,  // 目标地址
				changeOrigin: true //设置同源  默认false，是否需要改变原始主机头为目标URL,
			},
			'/micro': {    // search为转发路径
				target: process.env.VUE_APP_MICRO_API,  // 目标地址
				pathRewrite:(() =>{
					return process.env.VUE_APP_IS_LOCAL ?
						{ '^/micro/interactivemall': '/' } :
						{ '^/micro': '/' }
				})(),/*{ '^/micro': '/' },*/
				changeOrigin: true //设置同源  默认false，是否需要改变原始主机头为目标URL,
			}
		}
	}
};
